<?php

require_once("animal.php");
require_once("ape.php");
require_once("frog.php");

$sheep = new Animal("shaun");

echo "Nama Hewan : $sheep->name <br>"; // "shaun"
echo "Jumlah Kaki : $sheep->legs <br>"; // 4
echo "Apakah Berdarah Dingin? : $sheep->cold_blooded <br><br>"; // "no"

$sungokong = new Ape("ape");

echo "Nama Hewan : $sungokong->name <br>"; // "ape"
echo "Jumlah Kaki : $sungokong->legs <br>"; // 2
echo "Apakah Berdarah Dingin? : $sungokong->cold_blooded <br>"; // "no"
echo "Jump : ";
$sungokong->yell();
echo "<br><br>";

$kodok = new Frog("buduk");

echo "Nama Hewan : $kodok->name <br>"; // "ape"
echo "Jumlah Kaki : $kodok->legs <br>"; // 2
echo "Apakah Berdarah Dingin? : $kodok->cold_blooded <br>"; // "no"
echo "Jump : ";
$kodok->jump();
